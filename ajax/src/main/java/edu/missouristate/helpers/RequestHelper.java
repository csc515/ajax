package edu.missouristate.helpers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class RequestHelper {

	public static String getTrimmedParametersFromRequest(HttpServletRequest request) {
		
		if (request == null)
			return "";
		
		StringBuilder params = new StringBuilder();
		Map<String, Object> parameters = getUniqueParametersFromRequest(request);
		
		// Loop through the parameters (some may be too long) and set the parameters with delimiters
		// on the adminLog object
		if (Helper.hasItems(parameters)) {
			for (Map.Entry<String, Object> entry : parameters.entrySet()) {
			    String key = entry.getKey();
			    Object value = entry.getValue();
			    
			    if (StringHelper.hasText(key) && value != null) {
				    if (StringHelper.hasText(params))
						params.append("|");
				    
				    if (value instanceof String) {
				    	String valueStr = (String)value;
				    	
				    	// If it is a long parameter, trim it and add ellipses
				    	valueStr = StringHelper.getTrimmedString(valueStr, 29, true);
				    	value = valueStr;
				    } else if (value instanceof String[]) {
				    	StringBuilder vals = new StringBuilder();
				    	
			    		for (String str : ((String[])value)) {
			    			str = StringHelper.getTrimmedString(str, 29, true);
			    			
			    			if (vals.toString().length() > 0)
			    				vals.append(",");
			    			
			    			vals.append(str);
						}
			    		
			    		value = vals.toString();
			    	}
				    
				    params.append(key+"~"+value);	
			    }
			}
		}
		
		String paramsStr = params.toString();
		
		if (paramsStr.length() > 2000)
			paramsStr = paramsStr.substring(0, 2000);
		
		return paramsStr;
	}
	
	public static Map<String, Object> getUniqueParametersFromRequest(HttpServletRequest request) {
		
		if (request == null)
			return new HashMap<String, Object>();
			
		Map<String, String[]> pm = request.getParameterMap();
	    Iterator<?> it = pm.entrySet().iterator();
	    Map<String, Object> params = new HashMap<String, Object>();
	    
	    while (it.hasNext()) {
	        Map.Entry<?,?> pairs = (Map.Entry<?,?>)it.next();
	        
	        if (pairs.getKey() instanceof String) {
	        	String key = (String)pairs.getKey();
	        	Object value = pairs.getValue();
	       		params.put(key, value);
	        }
	    }
	    
	    return params;
	}
	
	public static String getBody(HttpServletRequest request) throws IOException {

	    String body = null;
	    StringBuilder stringBuilder = new StringBuilder();
	    BufferedReader bufferedReader = null;
	    BufferedReader reader = null; 
	    		
	    try {
	        reader = request.getReader();
	        
	        if (reader != null) {
	            char[] charBuffer = new char[128];
	            int bytesRead = -1;
	            while ((bytesRead = reader.read(charBuffer)) > 0) {
	                stringBuilder.append(charBuffer, 0, bytesRead);
	            }
	        } else {
	            stringBuilder.append("");
	        }
	        
//	        InputStream inputStream = request.getInputStream();
//	        
//	        if (inputStream != null) {
//	            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//	            char[] charBuffer = new char[128];
//	            int bytesRead = -1;
//	            while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
//	                stringBuilder.append(charBuffer, 0, bytesRead);
//	            }
//	        } else {
//	            stringBuilder.append("");
//	        }
	    } catch (IOException ex) {
	        throw ex;
	    } finally {
	        if (bufferedReader != null) {
	            try {
	                bufferedReader.close();
	            } catch (IOException ex) {
	                //throw ex;
	            }
	        }
	        if (reader != null) {
	            try {
	                reader.close();
	            } catch (IOException ex) {
	                //throw ex;
	            }
	        }
	    }

	    body = stringBuilder.toString();
	    return body;
	}
}
