<!doctype html>
<html lang="en">
<head>
	<title>Ajax Create Example</title>
	<!-- Bootstrap 4.5 CSS-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">	
	<!-- Bootstrap JS Requirements -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
	<style>
		table, tr, th, td {
			border: 1px solid #000;
			width: 200px;
		}
		body {
			padding: 10px;
		}
	</style>
</head>	
<body>
	<h1 id="heading"></h1>
	
	<div class="row">
		<div class="col-6">
			<form>
				<div class="form-group row">
					<!-- <label for="name" class="col-sm-2 col-form-label">Email</label> -->
				  	<div class="col-sm-10">
				    	<input type="text" id="firstName" value="" placeholder="First Name" />
				    	<input type="text" id="lastName" value="" placeholder="Last Name" class="mt-2" />
				  	</div>
				</div>
				<div class="form-group row">
					<button id="btn" type="button" class="btn btn-success ml-3">Submit</button>
				</div>
			</form>		
		</div>
	</div>
	
	<script>
		var btn, firstNameElem, lastNameElem;
		
		// Wait for the DOM to be loaded
		window.addEventListener("DOMContentLoaded", ()=> {
			// Initialize Variables
			btn = document.getElementById("btn");
			firstNameElem = document.getElementById("firstName");
			lastNameElem = document.getElementById("lastName");

			// Process Event Listeners
			addEventListeners();
			
			// Call functions
			populateHeading();
		});
		
		// Populate Heading
		function populateHeading() {
			let heading = document.getElementById("heading");
			let textNode = document.createTextNode("Ajax Create Example");
			heading.appendChild(textNode);
		}
		
		// Add Event Listeners Function
		function addEventListeners() {
			btn.addEventListener("click", ()=>{
				postUser();	
			});		
		}
		
		// Could build a GET call to the Server and add query string parameters
		// foo.com?k=v&k=v&k=v&k=v&k=v&k=v&k
		
		// Post User Function
		function postUser() {
			let data = {};
			data.firstName = firstNameElem.value;
			data.lastName = lastNameElem.value;
			
			fetch('/postuserobject', {
				  method: 'POST', // The method
				  //mode: 'no-cors', // It can be no-cors, cors, same-origin
				  //credentials: 'same-origin', // It can be include, same-origin, omit
				  headers: {
				    'Content-Type': 'application/json', // Your headers
				  },
				  body: JSON.stringify(data),  // Payload of the request
			})
			.then((httpresponseservlet) => {
				if (httpresponseservlet.ok) {
					return httpresponseservlet.json();	
				} else {
					console.log("Bad Response: " + httpresponseservlet.ok);	
				}
			})
			.then(function(RequestResponse) {			    
			    if (RequestResponse.successMessage.length > 0) {
			    	alert(RequestResponse.successMessage);
			    	location.href="/read";
			    }
			})
			.catch(function(error) {
			    // If there is any error you will catch them here
			    console.log(`${error}`);
			});
		}
		
		
	</script>
</body>
</html>