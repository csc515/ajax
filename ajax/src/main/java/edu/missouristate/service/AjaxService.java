package edu.missouristate.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.missouristate.repository.AjaxRepository;
// Spring MVC (R)
// BUSINESS LOGIC LIVES HERE
// When the server starts, the Dispatcher Servlet (DS) will call AjaxService ajaxService = new AjaxService();
// The DS will then place the object in memory  fancy word... sit down ... (loaded into the Spring IOC Container)
// IOC = Inversion of Control
// ajaxService is in memory and it is a Singleton
// Singleton - is a Java Class that is created/instantiated once and then used many times

@Service("ajaxService")
public class AjaxService {

	@Autowired
	AjaxRepository ajaxRepo;
	
	public List<String> getUsers() {
		return ajaxRepo.getUsers();
	}	
	
}
