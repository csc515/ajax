<!doctype html>
<html lang="en">
<head>
	<title>Ajax Read Example</title>
	<!-- Bootstrap 4.5 CSS-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">	
	<!-- Bootstrap JS Requirements -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
	<style>
		table, tr, th, td {
			border: 1px solid #000;
			width: 200px;
		}
		body {
			padding: 10px;
		}
	</style>
</head>	
<body>
	<h1 id="heading"></h1>
	
	<div class="row">
		<div class="col-sm-5">
			<table class="table table-hover table-striped table-bordered">
				<thead>
					<tr>
						<th>First Name</th>
						<th>Last Name</th>
					</tr>
				</thead>
				<tbody id="tbody">
				</tbody>
			</table>
		</div>
	</div>
	
	<script>
		var tbody;
		
		// Wait for the DOM to be loaded
		window.addEventListener("DOMContentLoaded", ()=> {
			tbody = document.getElementById("tbody");
			populateHeading();
			fetchUserObjects(); 
		});
		
		function fetchUserObjects() {
			fetch("/crud/getuserobjects")
			// When successful, we receive a response object 
			// We need to use the JSON method to get the data from the object
			.then((httpresponseservlet) => {  // PROMISE
				if (httpresponseservlet.ok) {  // status code of 200 (OK)
					return httpresponseservlet.json();	// Get another Promise back
				} else {
					console.log("Bad Response: " + httpresponseservlet.ok);	
				}
			})
			.then(function(RequestResponse) {
				let list = RequestResponse.list;
				
			    // Here you get the data to modify as you please
			    //console.log("our data: ", data);
			    list.forEach((user)=> {
			    	console.log("user.firstName = " + user.firstName +
			    			" user.lastName = " + user.lastName);
			    });
			    
			    populateTable(list);
			})
			.catch(function(error) {
			    // If there is any error you will catch them here
			    console.log(`${error}`);
			});
		}
		
		function populateHeading() {
			let heading = document.getElementById("heading");
			let textNode = document.createTextNode("Ajax Read Example");
			heading.appendChild(textNode);
		}
				
		function populateTable(userList) {
			if (userList == null || typeof userList == undefined ||
					userList.length == 0) {
				let tr = document.createElement("tr");
				let td = document.createElement("td");
				let text = document.createTextNode("No Data Found");
				td.setAttribute("colspan", "2");
				td.appendChild(text);
				tr.appendChild(td);
				tbody.appendChild(tr);
			} else {
				tbody.innerHTML = ""; // empty the table rows
				
				userList.forEach((user)=>{
					let tr = getRecord(user.firstName, user.lastName);
					tbody.appendChild(tr);	
				});
			}
		}
			
		function getRecord(firstName, lastName) {
			let tr = document.createElement("tr"); 
			let td1 = document.createElement("td");
			let text1 = document.createTextNode(firstName);
			let td2 = document.createElement("td");
			let text2 = document.createTextNode(lastName);
			td1.appendChild(text1);
			td2.appendChild(text2);
			tr.appendChild(td1);
			tr.appendChild(td2);
			return tr;
		}
	</script>
</body>
</html>