package edu.missouristate.helpers;

public class StringHelper {

	public static boolean hasText(String str) {
		return str != null && str.trim().length() > 0;
	}

	public static boolean hasText(StringBuilder sb) {
		if (sb != null) {
			return hasText(sb.toString());
		}

		return false;
	}
	
	public static String getTrimmedString(String string, Integer length, Boolean addEllipses) {
		
		if (!hasText(string) || length == null || addEllipses == null && length > 0)
			return string;
		
    	if (hasText(string) && string.length() > length) {
   			string = string.substring(0, length);
    		
    		if (addEllipses == true)
    			string += "...";
    	}
    	
    	return string;
	}
	
}
