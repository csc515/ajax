package edu.missouristate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.missouristate.helpers.RequestHelper;
import edu.missouristate.model.RequestResponse;
import edu.missouristate.model.User;
import edu.missouristate.service.AjaxService;

@Controller
public class AjaxController {
	private List<String> userList = new ArrayList<String>();
	private List<User> userObjectList = new ArrayList<User>();
	
	@Autowired
	AjaxService ajaxService;
	
	@PostConstruct
	public void init() {
		userList.add("Bob");
		userList.add("Rajesh");
		userList.add("Susan");
		userObjectList.add(new User("Bob", "E"));
		userObjectList.add(new User("Rajesh", "D"));
		userObjectList.add(new User("Susan", "A"));
	}
	
	@GetMapping(value="/") // localhost:8080/
	public String getIndex() {
		return "index";
	}
	
	@GetMapping(value="/example2")
	public String getExample2() {
		return "example2";
	}
	
	@GetMapping(value="/example3")
	public String getExample3() {
		return "example3";
	}
	
	@ResponseBody
	@GetMapping(value="/getusers")   // http://localhost:8080/getusers   /getusers is the endpoint
	public List<String> getUsers() {
		// Real Application
		//return ajaxService.getUsers();
		
		return userList;
	}
	
	@ResponseBody
	@GetMapping(value="/getuserbyid")
	public String getUserById(Integer id) {
		String user = "";
		
		try {
			user = userList.get(id);
		} catch (ArrayIndexOutOfBoundsException e) {
			/* do nothing */
		}
		
		return user;
	}
	
	@ResponseBody
	@GetMapping(value="/getuserobjects")
	public RequestResponse getUserObjects() {             // {"firstName":"Bob", "lastName":"E" ...} or [{},{}]
		RequestResponse reqRes = new RequestResponse();
		reqRes.setList(userObjectList);
		return reqRes;
	}
	
	@ResponseBody // always use this for AJAX requests / Web Services
	@PostMapping(value="/postuser")   
	public RequestResponse postName(HttpServletRequest req, @RequestBody RequestResponse reqRes, String firstName, 
			String lastName) {
		// reqRes = Data Transfer Object (DTO)
		try {
			//String params = RequestHelper.getTrimmedParametersFromRequest(req);
			//System.out.println("Params: " + params);
			
			//String body = RequestHelper.getBody(req);
			//System.out.println("Body: " + body);
			
			if (reqRes != null && 
					reqRes.getFirstName() != null &&
					reqRes.getLastName() != null) {
				userObjectList.add(
						new User(reqRes.getFirstName(), reqRes.getLastName()));
				reqRes.setSuccessMessage("User Added Successfully");
				reqRes.setList(userObjectList);
			}
			
			return reqRes;	
		} catch (Exception e) {
			RequestResponse rr = new RequestResponse();
			rr.setErrorMessage(e.getMessage());
			return rr;
		}
	}
	
	@ResponseBody
	@GetMapping(value="/getuserobjectbyid")
	public User getUserObjectById(Integer id) {
		User user = null;
		
		try {
			user = userObjectList.get(id);
		} catch (ArrayIndexOutOfBoundsException e) {
			/* do nothing */
		}
		
		return user;
	}
	
}
