<!doctype html>
<html lang="en">
<head>
	<title>Ajax Examples</title>
	<!-- Bootstrap 4.5 CSS-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">	
	<!-- Bootstrap JS Requirements -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
	<style>
		table, tr, th, td {
			border: 1px solid #000;
			width: 200px;
		}
		body {
			padding: 10px;
		}
	</style>
</head>	
<body>
	<h1 id="heading"></h1>
	
	<div class="row">
		<div class="col-6">
			<form>
				<div class="form-group row">
					<!-- <label for="name" class="col-sm-2 col-form-label">Email</label> -->
				  	<div class="col-sm-10">
				    	<input type="text" id="firstName" value="" placeholder="First Name" />
				    	<input type="text" id="lastName" value="" placeholder="Last Name" class="mt-2" />
				  	</div>
				</div>
				<div class="form-group row">
					<button id="btn" type="button" class="btn btn-success ml-3">Submit</button>
				</div>
			</form>		
		</div>
	</div>
	
	<div class="row">
		<div class="col-4">
			<table class="table table-hover table-striped table-bordered">
				<thead>
					<tr>
						<th>FIRST NAME</th>
						<th>LAST NAME</th>
					</tr>
				</thead>
				<tbody id="tbody">
				</tbody>
			</table>
		</div>	
	</div>
	
	<script>
		var debug = 1;
		var tbody = "";
		var btn, firstNameElem, lastNameElem;
		
		// Wait for the DOM to be loaded
		window.addEventListener("DOMContentLoaded", ()=> {
			// Declare Variables
			btn = document.getElementById("btn");
			tbody = document.getElementById("tbody");
			firstNameElem = document.getElementById("firstName");
			lastNameElem = document.getElementById("lastName");
			
			// Process Event Listeners
			addEventListeners();
			
			// Call functions
			populateHeading();
			populateTable();
			fetchUserObjects(); 
		});
		
		function addEventListeners() {
			btn.addEventListener("click", ()=>{
				postUser();	
			});		
		}
		
		function postUser() {
			let data = {};
			data.firstName = firstNameElem.value;
			data.lastName = lastNameElem.value;
			
			fetch('/postuser', {
				  method: 'POST', // The method
				  //mode: 'no-cors', // It can be no-cors, cors, same-origin
				  //credentials: 'same-origin', // It can be include, same-origin, omit
				  headers: {
				    'Content-Type': 'application/json', // Your headers
				  },
				  body: JSON.stringify(data),  // Payload of the request
			})
			.then((httpresponseservlet) => {
				if (httpresponseservlet.ok) {
					return httpresponseservlet.json();	
				} else {
					console.log("Bad Response: " + httpresponseservlet.ok);	
				}
			})
			.then(function(RequestResponse) {
			    // Here you get the data to modify as you please
			    let userList = RequestResponse.list;
			    
			    userList.forEach((item)=> {
			    	console.log("item.firstName = " + item.firstName);
			    });
			    
			    populateTable(userList);
			    
			    if (RequestResponse.successMessage.length > 0) {
			    	alert(RequestResponse.successMessage);
			    }
			})
			.catch(function(error) {
			    // If there is any error you will catch them here
			    console.log(`${error}`);
			});
		}
		
		function populateHeading() {
			let heading = document.getElementById("heading");
			let textNode = document.createTextNode("Ajax Examples");
			heading.appendChild(textNode);
		}

		function fetchUserObjects() {
			let firstName = firstNameElem.value;
			let lastName = lastNameElem.value;
			let url = "/postUser?firstName="+firstName+"&lastName="+lastName;
			fetch("/postUser?firstName=&lastName=")
			// When successful, we receive a response object 
			// We need to use the JSON method to get the data from the object
			.then((httpresponseservlet) => {
				if (httpresponseservlet.ok) {
					return httpresponseservlet.json();	
				} else {
					console.log("Bad Response: " + httpresponseservlet.ok);	
				}
			})
			.then(function(RequestResponse) {
			    // Here you get the data to modify as you please
			    let userList = RequestResponse.list;
			    
			    userList.forEach((item)=> {
			    	console.log("item.firstName = " + item.firstName);
			    });
			    
			    populateTable(userList);
			})
			.catch(function(error) {
			    // If there is any error you will catch them here
			    console.log(`${error}`);
			});
		}
		
		
		function fetchUserObjects() {
			fetch("<%=request.getContextPath()%>/getuserobjects")
			// When successful, we receive a response object 
			// We need to use the JSON method to get the data from the object
			.then((httpresponseservlet) => {
				if (httpresponseservlet.ok) {
					return httpresponseservlet.json();	
				} else {
					console.log("Bad Response: " + httpresponseservlet.ok);	
				}
			})
			.then(function(RequestResponse) {
			    // Here you get the data to modify as you please
			    let userList = RequestResponse.list;
			    
			    userList.forEach((item)=> {
			    	console.log("item.firstName = " + item.firstName);
			    });
			    
			    populateTable(userList);
			})
			.catch(function(error) {
			    // If there is any error you will catch them here
			    console.log(`${error}`);
			});
		}
		
		function populateTable(userList) {
			if (userList == null || typeof userList == undefined ||
					userList.length == 0) {
				let tr = document.createElement("tr");
				let td = document.createElement("td");
				let text = document.createTextNode("No Data Found");
				td.setAttribute("colspan", "2");
				td.appendChild(text);
				tr.appendChild(td);
				tbody.appendChild(tr);
			} else {
				tbody.innerHTML = "";
				userList.forEach((item)=>{
					let tr = getRecord(item.firstName, item.lastName);
					tbody.appendChild(tr);	
				});
			}
		}
			
		function getRecord(firstName, lastName) {
			let tr = document.createElement("tr");
			let td1 = document.createElement("td");
			let text1 = document.createTextNode(firstName);
			let td2 = document.createElement("td");
			let text2 = document.createTextNode(lastName);
			td1.appendChild(text1);
			td2.appendChild(text2);
			tr.appendChild(td1);
			tr.appendChild(td2);
			return tr;
		}
	</script>
</body>
</html>