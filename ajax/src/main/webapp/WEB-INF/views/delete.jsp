<!doctype html>
<html lang="en">
<head>
	<title>Ajax Read Example</title>
	<!-- Bootstrap 4.5 CSS-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">	
	<!-- Bootstrap JS Requirements -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
	<style>
		table, tr, th, td {
			border: 1px solid #000;
			width: 200px;
		}
		body {
			padding: 10px;
		}
	</style>
</head>	
<body>
	<h1 id="heading"></h1>
	
	<div class="row">
		<div class="col-sm-5">
			<select id="select">
				
			</select>
			<button class="btn btn-danger">Delete</button>
		</div>
	</div>
	
	<script>
		var select;
		
		// Wait for the DOM to be loaded
		window.addEventListener("DOMContentLoaded", ()=> {
			select = document.getElementById("select");
			populateHeading();
			fetchUserObjects();
		});
		
		function fetchUserObjects() {
			fetch("/crud/getuserobjects")
			// When successful, we receive a response object 
			// We need to use the JSON method to get the data from the object
			.then((httpresponseservlet) => {
				if (httpresponseservlet.ok) {
					return httpresponseservlet.json();	
				} else {
					console.log("Bad Response: " + httpresponseservlet.ok);	
				}
			})
			.then(function(RequestResponse) {
				let list = RequestResponse.list;
			    populateDropdown(list);
			})
			.catch(function(error) {
			    // If there is any error you will catch them here
			    console.log(`${error}`);
			});
		}
		
		function populateHeading() {
			let heading = document.getElementById("heading");
			let textNode = document.createTextNode("Ajax Read Example");
			heading.appendChild(textNode);
		}
				
		function populateDropdown(userList) {
			select.innerHTML = "";
			
			userList.forEach((user)=>{
				let option = document.createElement("option");
				option.setAttribute("data-name", (user.firstName + user.lastName));
				option.setAttribute("value", (user.firstName + user.lastName));
				select.appendChild(option);	
			});
		}
			
	</script>
</body>
</html>