package edu.missouristate.helpers;

import java.util.List;
import java.util.Map;

public class Helper {

	public static boolean hasItems(Object[] array) {

		if (array != null && array.length > 0)
			return true;

		return false;
	}

	public static boolean hasItems(List<?> list) {

		if (list != null && list.size() > 0)
			return true;

		return false;
	}

	public static boolean hasItems(Map<?, ?> map) {

		if (map != null && map.size() > 0)
			return true;

		return false;
	}

}
